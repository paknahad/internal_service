package quote

import (
	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	"internal_service/application/auth"
	"net/http"
	"time"
)

type (
	Quote struct {
		gorm.Model
		Sender                string          `json:"sender,omitempty"`
		SenderProfile         *auth.JwtClaims `json:"sender_profile"`
		SourceCurrency        string          `json:"source_currency" validate:"required,iso4217"`
		TargetCurrency        string          `json:"target_currency" validate:"required,iso4217"`
		Amount                float64         `json:"amount" validate:"required,gt=0"`
		Fee                   float64         `json:"fee"`
		EstimatedDeliveryTime time.Duration   `json:"estimated_delivery_time"`
	}

	Validator struct {
		Validator *validator.Validate
	}
)

func (cv *Validator) Validate(i interface{}) error {
	if err := cv.Validator.Struct(i); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return nil
}

func LoadQuote(ctx echo.Context) (*Quote, error) {
	q := new(Quote)
	err := ctx.Bind(q)
	if err != nil {
		return nil, echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	if err = ctx.Validate(q); err != nil {
		return nil, err
	}

	user := ctx.Get("user").(*jwt.Token)
	q.SenderProfile = user.Claims.(*auth.JwtClaims)
	q.Sender = q.SenderProfile.Id
	q.CreatedAt = time.Now()

	return q, nil
}
