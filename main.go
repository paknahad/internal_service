package main

import (
	"context"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"internal_service/application"
	"internal_service/application/database"
	"internal_service/application/logger"
	"internal_service/application/middleware"
	"internal_service/application/redis"
	"internal_service/payment"
	"internal_service/quote"
	"net/http"
)

func main() {
	app := &application.App
	app.Build("config.yml", context.Background())
	log := app.Log

	redis.Initialize(app)
	database.Connect(app)
	defer app.DB.Close()

	e := echo.New()
	logger.Logger = log
	e.Use(logger.Hook())
	e.Use(middleware.Auth(app.Config))
	e.Use(middleware.RateLimit(app))

	e.Validator = &quote.Validator{Validator: validator.New()}

	e.POST("/quote", func(c echo.Context) error {
		q, err := quote.LoadQuote(c)
		if err != nil {
			log.Error(err)
			return err
		}

		q.Fee, q.EstimatedDeliveryTime, err = payment.CalculateFeeAndEstimatedTime(q)
		if err != nil {
			log.Error(err)
			return err
		}

		d := app.DB.Create(q)
		if d.Error != nil {
			log.Error(err)
			return err
		}

		q.Sender = ""
		return c.JSON(http.StatusOK, q)
	})

	addr := fmt.Sprintf("%s:%s", app.Config.Server.Host, app.Config.Server.Port)
	e.Logger.Fatal(e.Start(addr))
}
