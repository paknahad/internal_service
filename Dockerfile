FROM golang:1.16 AS build

WORKDIR /go/src/app
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .
RUN go build .


FROM ubuntu AS final

COPY --from=build /go/src/app/internal_service /
COPY --from=build /go/src/app/config.yml /
EXPOSE 80
ENTRYPOINT ["/internal_service"]
