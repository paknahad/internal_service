# Internal Service
## Run
```sh
docker-compose up -d
go mod vendor
go run .
```
## Test
```sh
curl -X POST \
  http://localhost:8080/quote \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyIiwibmFtZSI6IkhhbWlkIFBha25haGFkIiwiZW1haWwiOiJocC5wYWtuYWhhZEBnbWFpbC5jb20ifQ.9vI1alutVXh172YtUGT0DVC8HAQ9PeYTu_bL2dAiRGc' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
	"source_currency":"USD",
	"target_currency":"EUR",
	"amount": 1000
}'
```