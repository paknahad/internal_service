package application

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v3"
	"os"
)

type Config struct {
	Redis struct {
		Address string `yaml:"address" envconfig:"REDIS_ADDRESS"`
		DB      int    `yaml:"db" envconfig:"REDIS_DB"`
		Pass    string `yaml:"pass" envconfig:"REDIS_PASS"`
	}
	Server struct {
		Port string `yaml:"port" envconfig:"SERVER_PORT"`
		Host string `yaml:"host" envconfig:"SERVER_HOST"`
	}
	Rate struct {
		Limit    int64 `yaml:"limit" envconfig:"RATE_LIMIT"`
		Duration int64 `yaml:"duration" envconfig:"RATE_LIMIT_DURATION"`
	}
	Auth struct{
		Secret string `yaml:"secret" envconfig:"AUTH_SECRET"`
	}
	Log struct {
		Level  string `yaml:"level" envconfig:"LOG_LEVEL"`
		Sentry string `yaml:"sentry" envconfig:"SENTRY"`
	}
}

func ReadConfig(filename string) (*Config, error) {
	var cfg Config

	file, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("reading config failed %w", err)
	}
	defer file.Close()

	decoder := yaml.NewDecoder(file)
	err = decoder.Decode(&cfg)

	if err != nil {
		return nil, fmt.Errorf("failed to decode config %w", err)
	}

	err = envconfig.Process("", &cfg)
	if err != nil {
		return nil, fmt.Errorf("failed to update config with envs %w", err)
	}

	return &cfg, nil
}
