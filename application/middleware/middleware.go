package middleware

import (
	"fmt"
	"github.com/golang-jwt/jwt"
	echoRedisRateLimit "github.com/ianprogrammer/echo-redis-rate-limit-storage"
	"github.com/labstack/echo/v4"
	echoMiddleware "github.com/labstack/echo/v4/middleware"
	"golang.org/x/time/rate"
	"internal_service/application"
	"internal_service/application/auth"
	"net/http"
	"time"
)

func Auth(cnf *application.Config) echo.MiddlewareFunc {
	config := echoMiddleware.JWTConfig{
		Claims:     &auth.JwtClaims{},
		SigningKey: []byte(cnf.Auth.Secret),
	}

	return echoMiddleware.JWTWithConfig(config)
}

func RateLimit(app *application.Application) echo.MiddlewareFunc {
	rdConf := &echoRedisRateLimit.RateLimiterRedisStoreConfig{
		Rate:        rate.Limit(app.Config.Rate.Limit),
		ExpiresIn:   time.Duration(app.Config.Rate.Duration) * time.Second,
		RedisClient: app.Redis,
	}
	config := echoMiddleware.RateLimiterConfig{
		Skipper: echoMiddleware.DefaultSkipper,
		Store:   echoRedisRateLimit.NewRedisLimitStore(app.Context, *rdConf),
		IdentifierExtractor: func(ctx echo.Context) (string, error) {
			user := ctx.Get("user").(*jwt.Token)
			claims := user.Claims.(*auth.JwtClaims)
			id := claims.Id
			return id, nil
		},
		ErrorHandler: func(context echo.Context, err error) error {
			return &echo.HTTPError{
				Code:     http.StatusForbidden,
				Message:  "error",
				Internal: err,
			}
		},
		DenyHandler: func(context echo.Context, identifier string, err error) error {
			return &echo.HTTPError{
				Code:     http.StatusTooManyRequests,
				Message:  fmt.Sprintf(
					"Too many request. you can't request quotes more than %d times during %d seconds",
					app.Config.Rate.Limit,
					app.Config.Rate.Duration,
				),
				Internal: err,
			}
		},
	}

	return echoMiddleware.RateLimiterWithConfig(config)
}
