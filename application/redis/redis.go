package redis

import (
	"context"
	"github.com/go-redis/redis/v8"
	"internal_service/application"
)

var redisClient *redis.Client

func Initialize(app *application.Application) {
	host := app.Config.Redis.Address
	pass := app.Config.Redis.Pass
	db := app.Config.Redis.DB

	client, err := newClient(app.Context, host, pass, db)
	if err != nil {
		app.Log.Fatalln(err)
	}

	app.Redis = client
}

func newClient(ctx context.Context, host, password string, dbNum int) (*redis.Client, error) {
	if redisClient != nil {
		return redisClient, nil
	}

	rdb := redis.NewClient(&redis.Options{
		Addr:     host,
		Password: password,
		DB:       dbNum,
	})

	_, err := rdb.Ping(ctx).Result()
	if err != nil {
		return nil, err
	}

	redisClient = rdb

	return redisClient, nil
}
