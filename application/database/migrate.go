package database

import (
	"github.com/jinzhu/gorm"
	"internal_service/quote"
)

func Migrate(db *gorm.DB) {
	db.AutoMigrate(&quote.Quote{})
}
