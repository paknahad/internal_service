package database

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"internal_service/application"
)

func Connect(app *application.Application) {
	db, err := gorm.Open("sqlite3", "./application/database/data.db")
	db.LogMode(true)

	if err != nil {
		app.Log.Fatal(err)
	}

	Migrate(db)

	app.DB = db
}
