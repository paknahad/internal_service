package application

import (
	"context"
	"github.com/go-redis/redis/v8"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"internal_service/application/logger"
	"log"
	"os"
)

type Application struct {
	Context context.Context
	Config  *Config
	Redis   *redis.Client
	Log     *logrus.Logger
	DB      *gorm.DB
}

var App Application

func (app *Application) Build(path string, ctx context.Context) {
	cfg, err := ReadConfig(path)
	if err != nil {
		log.Panic(err)
	}

	app.Config = cfg
	app.Context = ctx
	app.Log = logger.NewLogger(os.Stderr, cfg.Log.Level, cfg.Log.Sentry)
}
