package logger

import (
	"io"
	"os"

	"github.com/sirupsen/logrus"
)

// New creates an instance of logger
func New(output io.Writer, formatter logrus.Formatter, level string, reportCaller bool, sentryDsn string) *logrus.Logger {
	logger := &logrus.Logger{
		Out:          output,
		Formatter:    formatter,
		Hooks:        make(logrus.LevelHooks),
		Level:        GetLevel(level),
		ExitFunc:     os.Exit,
		ReportCaller: reportCaller,
	}

	registerSentryHook(logger, sentryDsn)
	return logger
}

func NewLogger(output io.Writer, logLevel string, sentryDsn string) *logrus.Logger {
	return New(
		output,
		new(logrus.TextFormatter),
		logLevel,
		false,
		sentryDsn,
	)
}

func GetLevel(level string) logrus.Level {
	parsedLevel, err := logrus.ParseLevel(level)
	if err != nil {
		return logrus.InfoLevel
	}

	return parsedLevel
}
