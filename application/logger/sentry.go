package logger

import (
	"github.com/sirupsen/logrus"

	"github.com/evalphobia/logrus_sentry"
)

func registerSentryHook(logger *logrus.Logger, dsn string) {
	if dsn == "" {
		return
	}
	hook, err := logrus_sentry.NewSentryHook(dsn, []logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
	})
	if err != nil {
		return
	}
	hook.StacktraceConfiguration.Enable = true
	hook.StacktraceConfiguration.IncludeErrorBreadcrumb = true
	logger.Hooks.Add(hook)
}
