package auth

import "github.com/golang-jwt/jwt"

type JwtClaims struct {
	Id    string `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
	jwt.StandardClaims
}
